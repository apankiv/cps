#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'alex'


from zadanie1.signal_generator import *
from zadanie1.signal_storage import SignalStorage
from zadanie2.quantize import Quantize
from zadanie2.sampling import Sampling
from zadanie2.extrapolate import Extrapolate
from zadanie2.dac_sinc import DACSinc
from zadanie2.interpolate import Interpolate
from zadanie3.convolution import Convolution
from zadanie3.high_pass_filter import HighPassFilter
from zadanie3.low_pass_filter import LowPassFilter
from zadanie3.really_awesome_sum import ReallyAwesomeSum
from zadanie3.simulator import Simulator

s = SignalStorage(100)

call = {
    'S1': RandomNoise,
    'S2': GaussianNoise,
    'S3': Sin,
    'S4': SinOneHalf,
    'S5': SinTwoHalf,
    'S6': Square,
    'S7': SquareSymmetric,
    'S8': Triangle,
    'S9': Skok,
    'S10': Pulse,
    'S11': PulseNoise
}


def add(args):
    name = args[0].upper()
    ar = args[1:len(args)]
    try:
        s.push_signal(call[name](ar))
    except:
        call[name].show_help()


def show(args):
    for i in list(map(int, args)):
        print(s.get_signal(i))


def list_all(args):
    sd = False

    if len(args) > 0:
        sd = True

    for i, j in zip(list(range(10)), s.get_signals()):
        print(str(i+1) + ": " + (str(j.name) if not sd else str(j.data)))


def general_help(args):
    if len(args) == 0:
        print('Usage:')
        print('\tAvailable commands')
        print('\t\tlist - lists all signals in signal container')
        print('\t\tadd [signal] [args] - adds signal to container')
        print('\t\tremove [index] - removes signal from container by index')
        print('\t\tsave [index] [filename] - writes signal to binary file')
        print('\t\tload [filename] - reads signal from binary file and adds it to container')
        print('\t\tplot [index_1] [index_2] ... - plots signal')
        print('\t\thelp - shows this help')
        print('\t\thelp signals - list all signals')
        print('\t\texit')
        print()
        print('\tAvailable operations on signal:')
        print('\t\tsum [index_1] [index_2] - counts sum of signals')
        print('\t\tdiff [index_1] [index_2] - counts difference of signals')
        print('\t\tprod [index_1] [index_2] - counts product')
        print('\t\tdiv [index_1] [index_2] - divides signals')
        print('\t\tshow [index] - shows signal data')
        print('\t\tstat [index] - shows signal stats')
        print('\t\thist [index] [k] - creates histogram')
        print('\t\tquantize [type] [index] [levels] [frequency]')
        print('\t\tsample [index] [frequency]')
        print('\t\textrapolate [index] [frequency]')
        print('\t\tinterpolate [index] [frequency]')
        print('\t\tsinc [index] [frequency]')
    else:
        if args[0] == 'signals':
            print('Available signals:')
            print('\tS1  - random noise')
            print('\tS2  - gaussian noise')
            print('\tS3  - sine wave')
            print('\tS4  - sine wave one half')
            print('\tS5  - sine wave two half')
            print('\tS6  - square signal')
            print('\tS7  - square symmetric signal')
            print('\tS8  - triangle signal')
            print('\tS9  - skok (?)')
            print('\tS10 - pulse')
            print('\tS11 - pulse noise')


def quantize(args):
    if args[0].upper() == 'Q1':
        s.push_signal(Quantize.q1([s.get_signal_obj(int(args[1])), int(args[2]), float(args[3])]))

    if args[0].upper() == 'Q2':
        s.push_signal(Quantize.q2([s.get_signal_obj(int(args[1])), int(args[2]), float(args[3])]))


def extrapolate(args):
    i, j = list(map(float, args))

    s.push_signal(Extrapolate(s.get_signal_obj(int(i)), j))


def interpolate(args):
    i, j = list(map(float, args))

    s.push_signal(Interpolate(s.get_signal_obj(int(i)), j))


def sinc(args):
    i, j = list(map(float, args))

    s.push_signal(DACSinc(s.get_signal_obj(int(i)), j))


def sample(args):
    s.push_signal(Sampling.sample_signal([s.get_signal_obj(int(args[0])), float(args[1])]))


def filter_signal(args):
    type = args[0]
    index, length, freq, cutoff = list(map(int, args[1:]))
    h = LowPassFilter(length, freq, cutoff) if type == 'low' else HighPassFilter(length, freq, cutoff)
    result = Convolution([i[1] for i in s.get_signal(index)], h.response)
    s.push_signal(result)


def awesome_sum(args):
    x, y = list(map(int, args))
    s.push_signal(ReallyAwesomeSum(s.get_signal_obj(x), s.get_signal_obj(y)))


def simulate(args):
    index = int(args[0])
    length, velocity, time = list(map(float, args[1:]))
    simulation = Simulator(s.get_signal_obj(index), length, velocity, time)
    simulation.run()

commands = {
    'list': list_all,
    'add': add,
    'remove': s.delete_signal,
    'load': s.load,
    'save': s.save,
    'sum': s.create_sum,
    'diff': s.create_diff,
    'prod': s.create_prod,
    'div': s.create_div,
    'show': show,
    'stat': s.stat,
    'plot': s.plot,
    'hist': s.hist,
    'help': general_help,
    'quantize': quantize,
    'sample': sample,
    'extrapolate': extrapolate,
    'interpolate': interpolate,
    'sinc': sinc,
    'filter': filter_signal,
    'asum': awesome_sum,
    'simulate': simulate,
}

# add S3 1 0.01 0.1 1000
# 3 zad: v o3 f2
# 4 zad: v f1 t2 s3

while True:
    a = input("signal processor > ")
    commandArray = a.split()

    if not len(commandArray) == 0:
        if commandArray[0] == 'exit':
            break
        else:
            try:
                commands[commandArray[0]](commandArray[1:len(commandArray)])
            except:
                if commandArray[0] in commands:
                    commands[commandArray[0]].show_help()
                else:
                    general_help([])
    else:
        continue
