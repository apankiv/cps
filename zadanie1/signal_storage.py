__author__ = 'alex'

import os
import pickle

from zadanie1.signal_generator import SignalGenerator
# from zadanie2.extrapolate import DACSinc


class SignalStorage:
    @staticmethod
    def show_help():
        print('No help here')

    def __init__(self, lim):
        self.signals = []
        self.limit = lim

    def get_signal_obj(self, index):
        return self.signals[index - 1]

    def get_signal(self, index):
        return self.signals[index - 1].data

    def get_signals(self):
        return self.signals

    @staticmethod
    def b_extend(signal, delta):
        t = [(0, 0) for _ in range(int(float(delta) / float(signal[1][0] - signal[0][0])))]
        t.extend(signal)
        return t

    def save(self, args):
        i, name = args
        i = int(i)
        s = self.get_signal_obj(i)
        handler = open(name, 'wb')
        pickle.dump(s, handler, -1)
        handler.close()

    def load(self, args):
        name = args[0]
        handler = open(name, 'rb')
        s = pickle.load(handler)
        handler.close()
        self.push_signal(s)

    def create_sum(self, args):
        i, j = list(map(int, args))
        s1 = self.get_signal(i)
        s2 = self.get_signal(j)

        if len(s1) != len(s2):
            delta = s1[0][0] - s2[0][0]

            if delta != 0:
                if delta < 0:
                    s2 = SignalStorage.b_extend(s2, abs(delta))
                else:
                    s1 = SignalStorage.b_extend(s1, abs(delta))

        if len(s1) != len(s2):
            delta = s1[-1][0] - s2[-1][0]

            if delta < 0:
                s1.extend([(0, 0) for _ in range(int(float(delta) / float(s1[1][0] - s1[0][0])))])
            else:
                s2.extend([(0, 0) for _ in range(int(float(delta) / float(s2[1][0] - s2[0][0])))])

        data = []

        for a, b in zip(s1, s2):
            x1, y1 = a
            x2, y2 = b
            data.append((x1, (y1 + y2)))

        self.push_signal(SignalGenerator(data))

    def create_diff(self, args):
        i, j = list(map(int, args))
        s1 = self.get_signal(i)
        s2 = self.get_signal(j)

        if len(s1) != len(s2):
            delta = s1[0][0] - s2[0][0]

            if delta != 0:
                if delta < 0:
                    s2 = SignalStorage.b_extend(s2, abs(delta))
                else:
                    s1 = SignalStorage.b_extend(s1, abs(delta))

        if len(s1) != len(s2):
            delta = s1[-1][0] - s2[-1][0]

            if delta < 0:
                s1.extend([(0, 0) for _ in range(int(float(delta) / float(s1[1][0] - s1[0][0])))])
            else:
                s2.extend([(0, 0) for _ in range(int(float(delta) / float(s2[1][0] - s2[0][0])))])

        data = []

        for a, b in zip(s1, s2):
            x1, y1 = a
            x2, y2 = b
            data.append((x1, (y1 - y2)))

        self.push_signal(SignalGenerator(data))

    def create_prod(self, args):
        i, j = list(map(int, args))
        s1 = self.get_signal(i)
        s2 = self.get_signal(j)

        if len(s1) != len(s2):
            delta = s1[0][0] - s2[0][0]

            if delta != 0:
                if delta < 0:
                    s2 = SignalStorage.b_extend(s2, abs(delta))
                else:
                    s1 = SignalStorage.b_extend(s1, abs(delta))

        if len(s1) != len(s2):
            delta = s1[-1][0] - s2[-1][0]

            if delta < 0:
                s1.extend([(0, 0) for _ in range(int(float(delta) / float(s1[1][0] - s1[0][0])))])
            else:
                s2.extend([(0, 0) for _ in range(int(float(delta) / float(s2[1][0] - s2[0][0])))])

        data = []

        for a, b in zip(s1, s2):
            x1, y1 = a
            x2, y2 = b
            data.append((x1, (y1 * y2)))

        self.push_signal(SignalGenerator(data))

    def create_div(self, args):
        i, j = list(map(int, args))
        s1 = self.get_signal(i)
        s2 = self.get_signal(j)

        if len(s1) != len(s2):
            delta = s1[0][0] - s2[0][0]

            if delta != 0:
                if delta < 0:
                    s2 = SignalStorage.b_extend(s2, abs(delta))
                else:
                    s1 = SignalStorage.b_extend(s1, abs(delta))

        if len(s1) != len(s2):
            delta = s1[-1][0] - s2[-1][0]

            if delta < 0:
                s1.extend([(0, 0) for _ in range(int(float(delta) / float(s1[1][0] - s1[0][0])))])
            else:
                s2.extend([(0, 0) for _ in range(int(float(delta) / float(s2[1][0] - s2[0][0])))])

        data = []

        for a, b in zip(s1, s2):
            x1, y1 = a
            x2, y2 = b
            if abs(y2) > 1.e-5:
                data.append((x1, (y1 / y2)))
            else:
                data.append((x1, (y1 / 1.e-5)))

        self.push_signal(SignalGenerator(data))

    def var(self, i):
        a = [y for x, y in self.get_signal(int(i - 1))]
        return sum(a)

    def stat(self, args):
        i = int(args[0])
        s = self.get_signal_obj(i)

        if not s.get_type() == 'z2':
            start, end = (0, len(s.data)-1)

            print('Avg.:      %f' % s.avg([start, end]))
            print('Abs avg.:  %f' % s.abs_avg([start, end]))
            print('Eff. val:  %f' % s.w_s([start, end]))
            print('Var.:      %f' % s.var([start, end]))
            print('Avg. pow.: %f' % s.avg_pow([start, end]))

        else:
            print('MSE:  %f' % s.mse())
            print('SNR:  %f' % s.snr())
            print('PSNR: %f' % s.psnr())
            print('MD:   %f' % s.md())

    def push_signal(self, sig):
        if len(self.signals) == self.limit:
            self.signals.pop(0)

        self.signals.append(sig)

    def delete_signal(self, args):
        index, = list(map(int, args))

        if len(self.signals) >= index:
            self.signals.pop(int(index) - 1)

    def gp_save(self, filename, ind):
        f = open(filename, 'w')

        for a, b in self.signals[ind - 1].data:
            f.write(str(a) + ' ' + str(b) + '\n')

        f.close()

    def plot(self, args):
        graphs = list(map(int, args))
        plot_string = ''

        for i in graphs:
            self.gp_save('gnuplt' + str(i) + '.tmp', i)
            plot_string += '\'gnuplt' + str(i) + '.tmp\' with ' + self.get_signal_obj(int(i)).plot_type + ', '
        os.system('gnuplot -e "plot ' + plot_string + '" -persist')

        for i in graphs:
            os.system('rm -rf gnuplt' + str(i) + '.tmp')

    def hist(self, args):
        i, k = list(map(int, args))
        s = self.get_signal(i)
        H = [0] * k
        V = [p[1] for p in s]
        v0, v1 = min(V), max(V)
        for v in V:
            H[min(k - 1, int(k * (v - v0) / (v1 - v0)))] += 1
        data = [(v0 + (v1 - v0) * (i + 0.5) / k, H[i] / len(s)) for i in range(k)]
        self.push_signal(SignalGenerator(data, type_name='histogram'))

    def restore_sinc(self, args):
        i, t = list(map(float, args))
        s = self.get_signal(int(i))
        self.push_signal(DACSinc(s, t))
