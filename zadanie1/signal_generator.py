__author__ = 'alex'

import math
from numpy import random
import numpy as np
import scipy.integrate as integrate


def f_range(x, y, jump):
    while x < y:
        yield x
        x += jump


def df_range(x, y, jump):
    i = 0
    while x < y:
        yield (x, i)
        x += jump
        i += 1


class SignalGenerator(object):
    name = 'Signal'
    plot_type = 'lines'

    def __init__(self, data=[], type_name='custom'):
        self.type_name = type_name
        self.avg_value = None
        self.abs_avg_value = None
        self.avg_pow_value = None
        self.w_s_value = None
        self.var_sig_value = None

        self.data = data

    @staticmethod
    def show_help():
        print('No help here')

    def get_type(self):
        return self.type_name

    @staticmethod
    def check(a, ts, i):
        if i < ts:
            return 0
        if i == ts:
            return a / 2
        return a

    def avg(self, args):
        start, end = list(map(int, args))

        if self.avg_value is None:
            self.avg_value = (1.0 / (end - start + 1)) * sum(self.data[start:end][1])

        return self.avg_value

    def abs_avg(self, args):
        start, end = list(map(int, args))

        if self.abs_avg_value is None:
            self.abs_avg_value = (1.0 / (end - start + 1)) * sum(list(map(abs, self.data[start:end][1])))

        return self.abs_avg_value

    def avg_pow(self, args):
        start, end = list(map(int, args))

        if self.avg_pow_value is None:
            self.avg_pow_value = (1.0 / (end - start + 1)) * sum([pow(i, 2) for i in self.data[start:end][1]])

        return self.avg_pow_value

    def var(self, args):
        return self.avg_pow(args) - self.avg(args) ** 2

    def w_s(self, args):
        start, end = list(map(int, args))

        if self.w_s_value is None:
            self.w_s_value = math.sqrt(self.avg_pow([start, end]))

        return self.w_s_value


class DiscreteSignal(SignalGenerator):
    def __init__(self):
        super().__init__()
        self.plot_type = 'points'

    @staticmethod
    def get_type():
        return 'discrete'


class Z2(SignalGenerator):
    type_name = 'z2'

    @staticmethod
    def show_help():
        print('Quantized is special signal type')

    def __init__(self):
        self.data = []
        self.signal = None
        self.mse_val = None
        self.snr_val = None
        self.psnr_val = None
        self.md_val = None

    def mse(self):
        if self.mse_val is None:
            self.mse_val = 1.0/len(self.data) * sum([math.pow(j - self.signal.get_val(i), 2) for i, j in self.data])

        return self.mse_val

    def snr(self):
        if self.snr_val is None:
            self.snr_val = 10 * math.log((sum([pow(i, 2) for _, i in self.data])/(len(self.data) * self.mse())), 10)

        return self.snr_val

    def psnr(self):
        if self.psnr_val is None:
            self.psnr_val = 10 * math.log(max(self.data, key=lambda item: item[1])[1]/self.mse(), 10)

        return self.psnr_val

    def md(self):
        if self.md_val is None:
            self.md_val = max(list(map(abs, [j - self.signal.get_val(i) for i, j in self.data])))

        return self.md_val

    def find_index(self, v):
        i = 0

        while self.signal.data[i][0] < v and i < len(self.signal.data):
            i += 1
            if i == len(self.signal.data):
                i -= 1
                break

        return i


class Quantized(Z2):
    def __init__(self, data=[], signal=None):
        super().__init__()
        self.data = data
        self.signal = signal


class Signal(SignalGenerator):
    def __init__(self):
        super().__init__()

    @staticmethod
    def get_type():
        return 'rare'

    def get_val(self, t):
        pass

    def avg(self, args):
        if self.avg_value is None:
            self.avg_value = 1.0 / (self.data[-1][0] - self.data[0][0]) * \
                             integrate.quad(lambda x: self.get_val(x), self.data[0][0], self.data[-1][0], limit=100000)[
                                 0]

        return self.avg_value

    def abs_avg(self, args):
        if self.abs_avg_value is None:
            self.abs_avg_value = 1.0 / (self.data[-1][0] - self.data[0][0]) * \
                                 integrate.quad(lambda x: math.fabs(self.get_val(x)), self.data[0][0], self.data[-1][0],
                                                limit=100000)[0]

        return self.abs_avg_value

    def avg_pow(self, args):
        if self.avg_pow_value is None:
            self.avg_pow_value = 1.0 / (self.data[-1][0] - self.data[0][0]) * \
                integrate.quad(lambda x: math.pow(self.get_val(x), 2), self.data[0][0], self.data[-1][0],
                               limit=100000)[0]

        return self.avg_pow_value

    def var(self, args):
        if self.var_sig_value is None:
            self.var_sig_value = 1.0 / (self.data[-1][0] - self.data[0][0]) * \
                integrate.quad(lambda x: math.pow(self.get_val(x) - self.avg(None), 2), self.data[0][0],
                               self.data[-1][0], limit=100000)[0]

        return self.var_sig_value

    def w_s(self, args):
        start, end = args

        if self.w_s_value is None:
            self.w_s_value = math.sqrt(self.avg_pow([start, end]))

        return self.w_s_value


class Square(Signal):
    name = 'Square'

    @staticmethod
    def show_help():
        print('Arguments: A, T, t_1, f, kw, end')

    def __init__(self, args):
        super().__init__()
        self.args = list(map(float, args))
        A, T, t1, f, kw, end = self.args
        self.data = [(i, self.get_val(i)) for i, _ in df_range(t1, end, 1.0 / f)]

    def get_val(self, t):
        A, T, t1, f, kw, end = self.args
        return A if t % T < kw * T else 0


class SquareSymmetric(Signal):
    name = 'Square symmetric'

    @staticmethod
    def show_help():
        print('Arguments: A, T, t_1, f, kw, end')

    def __init__(self, args):
        super().__init__()
        self.args = list(map(float, args))
        A, T, t1, f, kw, tf = self.args
        self.data = [(i, self.get_val(i)) for i, _ in df_range(t1, tf, 1.0 / f)]

    def get_val(self, t):
        A, T, t1, f, kw, tf = self.args
        return A if t % T < kw * T else -A


class RandomNoise(DiscreteSignal):
    name = 'Random noise'

    @staticmethod
    def show_help():
        print('Arguments: start, end, A, f')

    @staticmethod
    def get_random_num(a):
        return random.randint(-1000 * a, 1000 * a) / 1000.0

    def __init__(self, args):
        super().__init__()
        start, end, A, f = list(map(float, args))
        self.data = [(i, RandomNoise.get_random_num(A)) for i in f_range(start, end, 1.0 / f)]


class GaussianNoise(DiscreteSignal):
    name = 'Gaussian noise'

    @staticmethod
    def show_help():
        print('Arguments: start, end, A, f')

    @staticmethod
    def generate_points(a, num):
        random_numbers = np.random.normal(0, 1, num)
        return [i * (a / max(abs(min(random_numbers)), max(random_numbers))) for i in random_numbers]

    def __init__(self, args):
        super().__init__()
        start, end, A, f = list(map(float, args))
        self.data = [(i, j) for i, j in
                     zip(f_range(start, end, 1.0 / f), GaussianNoise.generate_points(A, (end - start) / (1.0 / f)))]


class Sin(Signal):
    name = 'Sin'

    @staticmethod
    def show_help():
        print('Arguments: A, T, end, f')

    def __init__(self, args):
        super().__init__()
        self.args = list(map(float, args))
        A, T, end, f = self.args
        self.args.append(0)
        start = 0
        self.data = [(i, self.get_val(i)) for i in f_range(start, end, 1.0 / f)]

    def get_val(self, t):
        A, T, end, f, start = self.args
        return A * math.sin((2 * math.pi) / T * (t - start))


class SinOneHalf(Signal):
    name = 'SinOneHalf'

    @staticmethod
    def show_help():
        print('Arguments: A, T, end, f')

    def __init__(self, args):
        super().__init__()
        self.args = list(map(float, args))
        A, T, end, f = self.args
        self.args.append(0)
        start = 0
        self.data = [(i, self.get_val(i)) for i in f_range(start, end, 1.0 / f)]

    def get_val(self, t):
        A, T, end, f, start = self.args
        return max(0, A * math.sin((2 * math.pi) / T * (t - start)))


class SinTwoHalf(Signal):
    name = 'SinTwoHalf'

    @staticmethod
    def show_help():
        print('Arguments: A, T, end, f')

    def __init__(self, args):
        super().__init__()
        self.args = list(map(float, args))
        A, T, end, f = self.args
        self.args.append(0)
        start = 0
        self.data = [(i, self.get_val(i)) for i in f_range(start, end, 1.0 / f)]

    def get_val(self, t):
        A, T, end, f, start = self.args
        return A * abs(math.sin((2 * math.pi) / T * (t - start)))


class Triangle(Signal):
    name = 'Triangle signal'

    @staticmethod
    def show_help():
        print('Arguments: A, T, t_1, f, kw, end')

    def see_help(self):
        print(self.name)

    def __init__(self, args):
        super().__init__()
        self.args = list(map(float, args))
        A, T, t1, f, kw, end = self.args

        self.data = [(i, self.get_val(i)) for i in f_range(t1, end, 1.0 / f)]

    def get_val(self, t):
        A, T, t1, f, kw, end = self.args

        in_period = t % T
        middle = kw * T

        if in_period < middle:
            return in_period / middle * A

        return (T - in_period) / (T - middle) * A


class Pulse(DiscreteSignal):
    name = 'Pulse'

    @staticmethod
    def show_help():
        print('Arguments: A, n_s, n_1, f')

    def __init__(self, args):
        super().__init__()
        A, ns, n1, f = list(map(float, args))
        self.data = [(i, (A if j == ns else 0)) for i, j in df_range(-n1, n1, 1.0 / f)]


class PulseNoise(DiscreteSignal):
    name = 'PulseNoise'

    @staticmethod
    def show_help():
        print('Arguments: A, t_1, d, f, p')

    def __init__(self, args):
        super().__init__()
        A, t1, d, f, p = list(map(float, args))
        self.data = [(i, (A if random.random() < p else 0)) for i, j in df_range(t1, t1 + d, 1.0 / f)]


class Skok(Signal):
    name = 'Skok'

    @staticmethod
    def show_help():
        print('Arguments: A, t_1, f, t_s')

    def __init__(self, args):
        super().__init__()
        self.args = list(map(float, args))
        A, t1, f, ts = self.args
        self.data = [(i, self.get_val(i)) for i in f_range(-t1, t1, 1.0 / f)]

    def get_val(self, t):
        A, t1, f, ts = self.args
        return SignalGenerator.check(A, ts, t)
