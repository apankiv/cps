__author__ = 'alex'

from zadanie3.blackman import *
from pylab import *
import math


class LowPassFilter(object):
    def __init__(self, length, sampling_frequency, cutoff_frequency):
        half = length / 2
        m = length
        black_man = BlackMan(m).indexes
        k = sampling_frequency / cutoff_frequency
        self.response = [0 for _ in range(int(m))]

        for i in range(int(half)):
            self.response[i] = black_man[i] * math.sin(
                (2 * math.pi * ((i - half) / k))
            ) / (math.pi * (i - half))

        self.response[int(half)] = black_man[int(half)] * 2. / k

        for i in range(int(half) - 1):
            self.response[int(half) + i + 1] = self.response[int(half) - i + 1]

    def plot_response(self):
        plot(self.response)
        show()
