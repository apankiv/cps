__author__ = 'alex'

from zadanie1.signal_generator import Signal, f_range


class ReallyAwesomeSum(Signal):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_discrete(self, x, y, frequency):
        return [self.x.get_val(i) + self.y.get_val(i) for i in f_range(x, y, 1.0 / frequency)]
