__author__ = 'alex'

from zadanie3.correlate import Correlate


class Simulator(object):
    def __init__(self, signal, length, velocity, time):
        self.end = 0.1
        self.frequency = 5000
        self.origin = signal
        self.signal = signal.get_discrete(0, self.end, self.frequency)
        self.moving_object = self.MovingObject(time, length, velocity)
        self.timeout = time

    def run(self):
        return_buffer = []

        while self.moving_object.distance > 0:
            correlated = Correlate(self.moving_object.next_move(signal=self.origin), self.signal).data
            return_buffer.append(correlated)
            # time.sleep(self.timeout)
            print('len of corelated %d' % (len(correlated)))
            print(correlated.index(max(correlated)) - float(len(self.signal)) / 2.0)
            print(len(return_buffer))

    class MovingObject(object):
        def __init__(self, delta, distance, velocity):
            self.time = 0
            self.time_delta = delta
            self.distance = distance
            self.velocity = velocity
            self.signal_speed = 340  # from internet

        def reflect_signal(self, signal):
            timeout = self.distance / self.signal_speed
            return signal.get_discrete(0 + timeout, 0.1 + timeout, 5000)

        def next_move(self, signal):
            result = None

            if self.distance > 0:
                result = self.reflect_signal(signal)
                self.distance -= self.time_delta * self.velocity

            return result
