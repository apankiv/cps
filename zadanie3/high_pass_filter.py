__author__ = 'alex'

from zadanie3.low_pass_filter import *


class HighPassFilter(object):
    def __init__(self, length, sampling_frequency, cutoff_frequency):
        lpf = LowPassFilter(length, sampling_frequency, (sampling_frequency / 2 - cutoff_frequency)).response

        for i in range(len(lpf)):
            if i % 2:
                lpf[i] = -lpf[i]

        self.response = lpf
