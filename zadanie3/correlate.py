__author__ = 'alex'

import numpy as np

from zadanie1.signal_generator import DiscreteSignal


class Correlate(DiscreteSignal):
    def __init__(self, x, h):
        self.h = h
        self.x = list(reversed(x))
        self.data = self.process()

    def process(self):
        data = []
        for n in range(len(self.h) + len(self.x) - 1):
            s = 0

            for k in range(len(self.h)):
                hv = self.h[k] if k < len(self.h) else 0
                xv = self.x[n-k] if 0 <= n - k < len(self.x) else 0
                s += hv*xv

            data.append((n, s))

        return data