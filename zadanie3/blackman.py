__author__ = 'alex'

import math


class BlackMan(object):
    def __init__(self, m):
        self.indexes = [0.42 - 0.5*math.cos(2*math.pi*n / m) + 0.08 * math.cos(4*math.pi*n/m) for n in range(m)]
