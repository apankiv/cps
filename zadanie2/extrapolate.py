__author__ = 'alex'

from zadanie1.signal_generator import *


class Extrapolate(Z2):
    type_name = 'z2'

    def __init__(self, signal, number_of_points):
        super().__init__()
        self.signal = signal
        self.end = signal.data[-1][0]
        self.number_of_points = int(number_of_points)
        self.t = self.signal.data[1][0] - self.signal.data[0][0]
        self.data = [(i, self.get_value(i)) for i in f_range(self.signal.data[0][0], self.signal.data[-1][0], 0.0001)]

    def get_value(self, t):
        index = self.find_index(t)
        return sum([self.signal.data[i][1] * Extrapolate.rect((t - self.t / 2.0 - i * self.t) / float(self.t))
                    for i in range(max(0, index - self.number_of_points),
                                   min(len(self.signal.data) - 1, index + self.number_of_points))])


    @staticmethod
    def rect(t):
        if abs(t) > 0.5:
            return 0
        if abs(t) == 0.5:
            return 0.5

        return 1
