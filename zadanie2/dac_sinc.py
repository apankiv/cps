__author__ = 'alex'

from zadanie1.signal_generator import *


class DACSinc(Z2):
    type_name = 'z2'

    def __init__(self, signal, number_of_points):
        super().__init__()
        self.signal = signal
        self.end = signal.data[-1][0]
        self.t = self.signal.data[1][0] - self.signal.data[0][0]
        self.number_of_points = int(number_of_points)
        self.data = [(i, self.get_value(i)) for i in f_range(self.signal.data[0][0], self.signal.data[-1][0], 0.0001)]

    def get_value(self, t):
        index = self.find_index(t)
        return sum([self.signal.data[i][1] * DACSinc.sinc(t / self.t - i)
                    for i in range(max(0, index - self.number_of_points), min(len(self.signal.data) - 1, index + self.number_of_points))])

    @staticmethod
    def sinc(t):
        return 1 if t == 1 else (math.sin(math.pi * t) / (math.pi * t) if not abs(t) < 0.00001 else 1)
