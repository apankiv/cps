__author__ = 'alex'

from zadanie1.signal_generator import SignalGenerator, f_range


class Sampling(object):
    @staticmethod
    def sample_signal(args):
        signal = args[0]
        f = float(args[1])

        return SignalGenerator(
            [(i, signal.get_val(i)) for i in f_range(signal.data[0][0], signal.data[-1][0], 1.0 / f)], 'z2')
