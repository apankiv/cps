__author__ = 'alex'

from zadanie1.signal_generator import *


class Quantize(object):
    @staticmethod
    def show_help():
        print('Quantize parameters are:')
        print('\t[index] - index of origin signal')
        print('\t[levels] - amount of levels')
        print('\t[frequency] - frequency of quantization')

    @staticmethod
    def q1(args):
        signal = args[0]
        levels = int(args[1])
        f = float(args[2])

        data = []
        multiplier = 1.0 / (
            (max(signal.data, key=lambda item: item[1])[1] - min(signal.data, key=lambda item: item[1])[1]) / float(
                levels - 1))

        for i in f_range(signal.data[0][0], signal.data[-1][0], 1.0 / f):
            data.extend([math.ceil(signal.get_val(i) * multiplier) / multiplier] * int(
                math.ceil((1.0 / f) / (signal.data[1][0] - signal.data[0][0]))))

        return Quantized(list(
            zip(f_range(signal.data[0][0], signal.data[-1][0],
                        1.0 / (int(math.ceil((1.0 / f) / (signal.data[1][0] - signal.data[0][0]))) * f)), data)),
            signal)

    @staticmethod
    def q2(args):
        signal = args[0]
        levels = int(args[1])
        f = float(args[2])

        data = []
        multiplier = 1.0 / (
            (max(signal.data, key=lambda item: item[1])[1] - min(signal.data, key=lambda item: item[1])[1]) / float(
                levels - 1))

        for i in f_range(signal.data[0][0], signal.data[-1][0], 1.0 / f):
            data.extend([math.ceil(signal.get_val(i) * multiplier) / multiplier if (signal.get_val(
                i) * multiplier) % 1 < -0.5 or (signal.get_val(i) * multiplier) % 1 >= 0.5 else math.floor(
                signal.get_val(i) * multiplier) / multiplier] * int(
                math.ceil((1.0 / f) / (signal.data[1][0] - signal.data[0][0]))))

        return Quantized(list(
            zip(f_range(signal.data[0][0], signal.data[-1][0],
                        1.0 / (int(math.ceil((1.0 / f) / (signal.data[1][0] - signal.data[0][0]))) * f)), data)),
            signal)
